# School

## TODO List
- Initialize Angular Framework
  - Actual School App
  - School Admin App
  - Product Admin App
  - Sandbox App

## Terminal Commands Used
### Creating 4 different applications (School, Admin, Super and Sandbox)

```shell
ng new school --skipInstall=true --minimal=true --createApplication=false --skipGit=true --style=scss --skipTests=true --prefix=sz --packageManager=yarn --newProjectRoot=frontend --directory=./
```
```shell
ng g application school --minimal=true --routing --style=scss --prefix=sz --skipTests=true
```
```shell
ng g application admin --minimal=true --routing --style=scss --prefix=sz --skipTests=true
```
```shell
ng g application super ---minimal=true --routing --style=scss --prefix=sz --skipTests=true
```
```shell
ng g application sandbox --minimal=true --routing --style=scss --prefix=sz --skipTests=true
```

# Problems you might run into

- Make sure to check the cli command

# Lecture

- Initialize Angular Front-end